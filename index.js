const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const txtFullName = document.querySelector('#span-full-name');

const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	txtFullName.innerHTML = `${firstName} ${lastName}`

}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);